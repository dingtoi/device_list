var express = require("express");
var androidDevices = require('android-device-list');
var router = express.Router();

router.post('/android/device/list', function (req, res) {
    var model = req.body.model;
    var devices = androidDevices.getDevicesByModel(model);
    if (devices.length > 0)
        res.json({ list: devices });
    else
        res.json({ list: [] });

});
router.post('/transaction/changeStatus', function (req, res) {
    var number = req.body.number;
    var status = req.body.status;
    var device = JSON.stringify(req.body.device);

    knex('transactions')
        .update({
            status: status,
            add_info: device
        })
        .where('number', number)
        .then(function (updated) {
            if (updated)
                res.json({ status: 200, data: updated });
            else
                res.json({ status: 500 });
        })
        .catch(function (error) {
            res.status(500).json();
        })
});
module.exports = router;